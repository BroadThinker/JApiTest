package com.penngo.common;

import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.handler.Handler;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.Sqlite3Dialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;
import com.penngo.api.ApiController;
import com.penngo.index.IndexController;
import com.penngo.model.Project;
import com.penngo.model.UseCase;
import com.penngo.util.Tool;

/**
 * API引导式配置
 */
public class ApiConfig extends JFinalConfig {
	public void configEngine(Engine me){

	}
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		System.out.println("=======configConstant");
		PropKit.use("config.txt");
		me.setDevMode(PropKit.getBoolean("devMode", false));
		me.setViewType(ViewType.FREE_MARKER);
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		System.out.println("=======configRoute");
		me.setBaseViewPath("/");
		me.add("/", IndexController.class, "/index");	// 第三个参数为该Controller的视图存放路径
		me.add("/api", ApiController.class);	
	}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
		c3p0Plugin.setDriverClass("org.sqlite.JDBC");
		c3p0Plugin.start();
		//DataSource dataSource = c3p0Plugin.getDataSource();
		//checkTable(dataSource);
		me.add(c3p0Plugin);
		
		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		me.add(arp);
		arp.setDialect(new Sqlite3Dialect());
//		c3p0Plugin.start();
//		arp.start();
		
//		Db.update(sql);
		arp.addMapping("project", Project.class);
		arp.addMapping("useCase", UseCase.class);
		//arp.start();
		
		
	}

	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		// 以下两行代码配置作用于控制层的全局拦截器
//		me.add(new JsonInterceptor());
//		me.addGlobalActionInterceptor(new JsonInterceptor());

		// 以下一行代码配置业务层全局拦截器
//		me.addGlobalServiceInterceptor(new JsonInterceptor());
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {
		me.add(new JsonHandle());
	}
	
	private void checkTable(DataSource dataSource) {
		try {
			
			String[] sqls = Tool.getSql();
			//System.out.println("configPlugin=========" + sql);
			Connection connection = dataSource.getConnection();
			
			for(String sql:sqls) {
				System.out.println("configPlugin=========" + sql);
				Boolean b = connection.prepareStatement(sql).execute();
				System.out.println("configPlugin=========" + b);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	class JsonHandle extends Handler {
		public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled){

			String contentType = request.getContentType();
			if(contentType != null && contentType.indexOf("application/x-www-form-urlencoded") > -1){
				String reqStr = HttpKit.readData(request);
				String[] strs = reqStr.split("&");
				if(strs.length > 0){
					for(String str:strs){
						String[] parames = str.split("=");
						String key = parames[0];
						String value = parames.length > 1 ? parames[1] : "";
						request.setAttribute(key, value);

					}
				}
			}
			next.handle(target, request, response, isHandled);
		}

	}
	class JsonInterceptor implements Interceptor {
		public void intercept(Invocation invocation){
		}
	}
}
