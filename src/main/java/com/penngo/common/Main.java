package com.penngo.common;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import com.jfinal.core.JFinal;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.server.undertow.UndertowServer;
import com.penngo.util.Tool;

public class Main {
	public static void initDb() {
		PropKit.use("config.txt");
		System.out.println(PropKit.get("jdbcUrl"));

		try {
			Class.forName("org.sqlite.JDBC");
			Connection conn =
					DriverManager.getConnection(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());

		    Statement stat = conn.createStatement();
		    //stat.executeUpdate("drop table if exists people;");
//		    stat.execute("");//.executeUpdate("create table people (name, occupation);");
			ResultSet rs = stat.executeQuery(" SELECT `name` FROM `sqlite_master` WHERE `type`='table'; ");
			Map<String, Boolean> existTable = new HashMap<>();
			while (rs.next()) {
				String name = rs.getString("name");
				existTable.put(name, true);
			}
			if(existTable.size() < 2){
				String[] sqls = Tool.getSql();
				//System.out.println("configPlugin=========" + sql);
				//Connection connection = dataSource.getConnection();

				for(String sql:sqls) {
					System.out.println("configPlugin=========" + sql);
					int b = conn.createStatement().executeUpdate(sql);
					//Boolean b = connection.prepareStatement(sql).execute();
					//Boolean b = stat.execute(sql);
					System.out.println("configPlugin=========" + b);
				}
			}

			conn.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 启动入口，运行此 main 方法可以启动项目，此 main 方法可以放置在任意的 Class 类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		UndertowServer.start(ApiConfig.class, 8000, true);
	}

}
